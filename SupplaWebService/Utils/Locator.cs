﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SupplaWebService.DataAccess.Interfaces;
using SupplaWebService.DataAccess.Repositories;
using System.Web.Configuration;

namespace SupplaWebService.Utils
{
    public static class Locator
    {
        public static IResponseRepository GetResponseRepository()
        {
            return new OleDbResponseRepository(
                WebConfigurationManager.ConnectionStrings[0].ConnectionString);
        }
    }
}