﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SupplaWebService.Utils;
using SupplaWebService.DataAccess.Models;

namespace SupplaWebService
{
    /// <summary>
    /// Web Service SOAP que recibe un código de barras como parámetro
    /// y retorna los siguientes datos: 
    /// string Barcode: Código de barras del documento 
    /// string SuraLot: Lote al que pertenece el documento (Si está en el catálogo)
    /// string ParentFile: Caja a la que pertenece el documento (Código de barras de la caja)
    /// string Location: Ubicación física en bodega en la que está almacenado el archivo padre
    /// </summary>
    [WebService(Namespace = "http://locahost.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class MainService : System.Web.Services.WebService
    {
        [WebMethod]
        public ResponseData GetResponseData(string barcode)
        {
            RequestData requestData = new RequestData() { Barcode = barcode };

            return Locator.GetResponseRepository().GetResponse(requestData);
        }
    }
}
