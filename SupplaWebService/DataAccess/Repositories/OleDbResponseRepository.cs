﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SupplaWebService.DataAccess.Interfaces;
using SupplaWebService.DataAccess.Models;

namespace SupplaWebService.DataAccess.Repositories
{
    public class OleDbResponseRepository : IResponseRepository
    {
        const string TableName = "CATALOGO_63";

        string _connectionString;

        public OleDbResponseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        //ToDo: Implement this!
        public ResponseData GetResponse(RequestData requestData)
        {
            return new ResponseData()
            {
                Barcode = requestData.Barcode,
                Location = "Dummy Location",
                ParentFile = "Parent",
                SuraLot = "1984"
            };
        }
    }
}
