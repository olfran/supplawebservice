﻿namespace SupplaWebService.DataAccess.Models
{
    public class ResponseData
    {
        public string Barcode { get; set; }
        public string SuraLot { get; set; }
        public string ParentFile { get; set; }
        public string Location { get; set; }
    }
}
