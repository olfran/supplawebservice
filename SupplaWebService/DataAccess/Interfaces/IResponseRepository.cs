﻿using SupplaWebService.DataAccess.Models;

namespace SupplaWebService.DataAccess.Interfaces
{
    public interface IResponseRepository
    {
        ResponseData GetResponse(RequestData requestData);
    }
}
