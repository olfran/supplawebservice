# Suppla Web Service #
Web Service SOAP para la empresa Suppla

### Descripción ###
Web Service SOAP que recibe un código de barras como parámetro y retorna los siguientes datos:

**string** *Barcode:* Código de barras del documento.

**string** *SuraLot:* Lote al que pertenece el documento (Si está en el catálogo).

**string** *ParentFile:* Caja a la que pertenece el documento (Código de barras de la caja).

**string** *Location:* Ubicación física en bodega en la que está almacenado el archivo padre.
